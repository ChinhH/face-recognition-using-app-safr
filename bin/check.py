#!/usr/bin/python
# Copyright 2018, RealNetworks, Inc

import sys
import requests
import socket
import colorama
from termcolor import colored, cprint

#setup
hostName="127.0.0.1"
protocol="http://"
baseURL=protocol + hostName

#init for windows term color
colorama.init()

#def processcheck():
#todo for ares

def printResults(name,status):
    if 'pass' in status.lower():
        cprint('{:26s} {:s}'.format(name, "PASS"), 'white', 'on_green', attrs=['bold'])
    elif 'fail' in status.lower():
        cprint('{:26s} {:s}'.format(name, "FAIL"), 'white', 'on_red', attrs=['bold'])
    else:
        cprint('{:26s} {:s}'.format(name, "????"), 'white', 'on_yellow', attrs=['bold'])

# verify the port is listening
def portCheck(name,port):
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            s.connect((hostName, int(port)))
            s.shutdown(2)
            printResults(name,"pass")
            return True
        except:
            printResults(name,"fail")
            return False
    except:
        printResults(name,"other")
        pass

# only check response code (2xx ok)
def simpleURLCheck(name,url):
    try:
        with requests.get(baseURL + url) as response:
            if 200 <= response.status_code <= 299:
                printResults(name,"pass")
                return True
            else:
                printResults(name,"fail")
                return False
    except (requests.exceptions.ConnectionError, requests.exceptions.Timeout, requests.exceptions.ProxyError):
        printResults(name,"fail")
        return False
    except:
        printResults(name,"other")
        pass

# must find searchString in response
def substringURLCheck(name,url,searchString):
    try:
        with requests.get(baseURL + url) as response:
            if 200 <= response.status_code <= 299:
                if searchString in response.text:
                    printResults(name,"pass")
                    return True
                else:
                    printResults(name,"fail")
                    return False
            else:
                printResults(name,"fail")
                return False
    except (requests.exceptions.ConnectionError, requests.exceptions.Timeout, requests.exceptions.ProxyError):
        printResults(name,"fail")
        return False
    except:
        printResults(name,"other")
        pass

print("\nSAFR Local Service Health")
print("-------------------------------")
portCheck('MongoDB Service', 27017)
simpleURLCheck('CoVi API Service', ':8080/covi-ws/version')
simpleURLCheck('Face Service', ':8888/facedetect')
simpleURLCheck('Object Storage Service', ':8086/health')
portCheck('Event Service', 8082)
simpleURLCheck('Virga', ':8084/health')
simpleURLCheck('Reports', ':8088/version')
print("")
