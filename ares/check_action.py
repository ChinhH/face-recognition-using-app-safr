import serial
import json
from datetime import datetime
import time
import argparse
port = "COM3"
baud = 9600

parser = argparse.ArgumentParser(description='Process some integers.')
parser.add_argument('--name', help='an integer for the accumulator')
args = parser.parse_args()
# import datetime
import os
NAME = args.name

checkdate = datetime.now().strftime("%Y-%m-%d")
checktime = datetime.now().strftime("%H-%M-%S")
if NAME is not None:
    with open('data.txt') as json_file:
        data = json.load(json_file)
        status = data['stt']
        # print(data)
        # print(status)
        if (status == '0'):
            ""# print('ignore')
        else:
            # print('run')

            # write stt 0 , current time to file
            datetime.now(tz=None)
            s = datetime.today().strftime('%Y-%m-%d-%H:%M:%S')
            # print(s)
            data = {'stt': '0', 'time': s}
            with open('data.txt', 'w') as outfile:
                json.dump(data, outfile)

            # write to serial
            ser = serial.Serial(port, baud, timeout=1)
            # open the serial port
            if ser.isOpen():
                # print(ser.name + ' is open...')
                ba = bytes([0x7b, 0x42, 0x41, 0x54, 0x7d])
                # print(ba)
                ser.write(ba)
                ser.close()

            # sleep 5s
            time.sleep(0.01)

            # write stt 1 , current time to file
            datetime.now(tz=None)
            s = datetime.today().strftime('%Y-%m-%d-%H:%M:%S')
            # print(s)
            data = {'stt': '1', 'time': s}
            with open('data.txt', 'w') as outfile:
                json.dump(data, outfile)
    if not os.path.exists("./txtvoice/" + NAME + ".txt"):
        with open("./txtvoice/" + NAME + ".txt", 'w', encoding='utf-8') as text:
            text.write(NAME + "-" + checkdate + ":" + checktime + '|' + "bạn "+ NAME + "|0L")
            text.close()
    else:
        with open("./txtvoice/" + NAME + ".txt", 'r', encoding='utf-8') as text:
            content = text.read()
            text.close()
            if content.split(':')[0] != NAME + "-" + checkdate:
                with open("./txtvoice/" + NAME + ".txt", 'w', encoding='utf-8') as text:
                    text.write(NAME + "-" + checkdate + ":" + checktime + '|' + "bạn "+ NAME + "|0B")
                    text.close()



    '''  
    ser = serial.Serial(port, baud, timeout=1)
        # open the serial port
    if ser.isOpen():
        print(ser.name + ' is open...')
        ba = bytes([0x7b,0x42,0x41,0x54,0x7d])
        print(ba)
        ser.write(ba)
        ser.close()
    '''
