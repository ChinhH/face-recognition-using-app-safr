# For GUI
import cv2,os
import tkinter as tk
import win32com.client as win32
from tkinter import filedialog
from tkinter.scrolledtext import ScrolledText

import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

from PIL import Image, ImageTk
from openpyxl import load_workbook
from pandas import ExcelWriter
import pandas as pd
import datetime

def get_ds(path_excel):
    DANHSACH = pd.read_excel(path_excel, sheet_name='base', dtype=object)
    DS_Member1 = []
    DS = []
    for i in range(len(DANHSACH['ACC'])):
        if str(DANHSACH['ACC'][i]) != 'nan':
            DS.append(DANHSACH['ACC'][i])
            DS_Member1.append([DANHSACH['ACC'][i], 0])
    return DS_Member1,DS

def send_email(message,subject,your_host,your_port,MY_ADDRESS,PASSWORD,To_email, CC_email):
    # set up the SMTP server
    s = smtplib.SMTP(host=your_host, port=your_port)
    s.starttls()
    s.ehlo()
    s.login(MY_ADDRESS, PASSWORD)
    # For each contact, send the email:
    # for name, email in zip(names, emails):
    msg = MIMEMultipart()  # create a message
    # add in the actual person name to the message template
    # message = message_template.substitute(PERSON_NAME=name.title())
    # Prints out the message body for our sake

    # setup the parameters of the message
    msg['From'] = MY_ADDRESS
    msg['To'] = To_email
    msg['CC'] = CC_email
    msg['Subject'] = subject

    # add in the message body
    msg.attach(MIMEText(message, 'plain'))

    # send the message via the server set up earlier.
    s.send_message(msg)
    del msg

    # Terminate the SMTP session and close the connection
    s.quit()

def fill_file_excel(filexl,ACC,Checkin,Checkout):
    dfOutput1 = pd.read_excel(filexl,  sheet_name='EXPORT',dtype=object)
    for i in range(0, 2000):
        if str(dfOutput1['ACC'][i]) == 'nan':
            dfOutput1.iat[i, 3] = ACC
            dfOutput1.iat[i, 1] = Checkin
            dfOutput1.iat[i, 11] = Checkout
            break
    for i in range(0, 2000):
        dfOutput1.iat[i, 2] = ('=IF(D{}="",IF(B{}="","","NGUOI_LA"),VLOOKUP(D{},base!$B$2:$J$101,2,0))').format(i + 2,
                                                                                                                i + 2,
                                                                                                                i + 2)
        dfOutput1.iat[i, 4] = ('=IF(D{}="","",VLOOKUP(D{},base!$B$2:$J$101,3,0))').format(i + 2, i + 2)
        dfOutput1.iat[i, 5] = ('=IF(C{}="NGUOI_LA","NO",IF(D{}="","",VLOOKUP(D{},base!$B$2:$J$101,4,0)))').format(i + 2,
                                                                                                                  i + 2,
                                                                                                                  i + 2)
        dfOutput1.iat[i, 6] = ('=IF(C{}="NGUOI_LA","NO",IF(D{}="","",VLOOKUP(D{},base!$B$2:$J$101,5,0)))').format(i + 2,
                                                                                                                  i + 2,
                                                                                                                  i + 2)
        dfOutput1.iat[i, 7] = ('=IF(C{}="NGUOI_LA","NO",IF(D{}="","",VLOOKUP(D{},base!$B$2:$J$101,6,0)))').format(i + 2,
                                                                                                                  i + 2,
                                                                                                                  i + 2)
        dfOutput1.iat[i, 8] = ('=IF(D{}="","",VLOOKUP(D{},base!$B$2:$J$101,7,0))').format(i + 2, i + 2)
        dfOutput1.iat[i, 9] = ('=IF(C{}="NGUOI_LA",O{},IF(D{}="","",VLOOKUP(D{},base!$B$2:$J$101,8,0)))').format(i + 2,
                                                                                                                 i + 2,
                                                                                                                 i + 2,
                                                                                                                 i + 2)
        dfOutput1.iat[i, 10] = ('=IF(C{}="NGUOI_LA",P{},IF(D{}="","",VLOOKUP(D{},base!$B$2:$J$101,9,0)))').format(i + 2,
                                                                                                                  i + 2,
                                                                                                                  i + 2,
                                                                                                                  i + 2)
    book = load_workbook(filexl)
    writer = ExcelWriter(filexl, engine='openpyxl')
    writer.book = book
    writer.sheets = dict((ws.title, ws) for ws in book.worksheets)
    dfOutput1.to_excel(writer, 'EXPORT', index=False, startrow=1, header=False)
    writer.save()

def checkin_checkout(acc_member,file_log,path_exl):
    y = datetime.datetime.now().strftime("%Y")
    m = datetime.datetime.now().strftime("%m")
    d = datetime.datetime.now().strftime("%d")
    H = datetime.datetime.now().strftime("%H")
    M = datetime.datetime.now().strftime("%M")
    S = datetime.datetime.now().strftime("%S")
    with open(file_log) as f:
        time_in = ""
        time_out =""
        for line in f:
            try:
                if str(line.split(' ')[0]) == 'XXX':
                    mon = line.split('-')[1]
                    day = line.split('-')[2].split(' ')[0]
                    hr = line.split(':')[0].split(' ')[2]
                    min = line.split(':')[1]
                    se = line.split(':')[2].split(' ')[0]
                    if m == mon and d == day:
                        ACC = line.split(' ')[6].split('\n')[0]
                        if ACC == str(acc_member[0]):
                            if int(acc_member[1]) == 0:
                                time_in = y + '-' + mon+'-' + day + " "+hr + ":" + min + ":"+ se
                                acc_member[1] = 1
                            time_out = y + '-' + mon+'-' + day + " "+hr + ":" + min + ":"+ se
                            acc_member[1] = 1
            except:
                pass
    if int(acc_member[1]) == 1:
        acc_member[1] = 0
        fill_file_excel(path_exl,str(acc_member[0]),time_in,time_out)

def get_checkin_member(acc_member, string, file_log):
    y = datetime.datetime.now().strftime("%Y")
    m = datetime.datetime.now().strftime("%m")
    d = datetime.datetime.now().strftime("%d")
    H = datetime.datetime.now().strftime("%H")
    M = datetime.datetime.now().strftime("%M")
    S = datetime.datetime.now().strftime("%S")
    with open(file_log) as f:
            for line in f:
                try:
                    if str(line.split(' ')[0]) == 'XXX':
                        mon = line.split('-')[1]
                        day = line.split('-')[2].split(' ')[0]
                        hr = line.split(':')[0].split(' ')[2]
                        min = line.split(':')[1]
                        if m == mon and d == day:
                            ACC = line.split(' ')[6].split('\n')[0]
                            if ACC == str(acc_member[0]):
                                if int(acc_member[1]) == 0:
                                    string = string  + str(acc_member[0])+":  Checkin lan dau tien -> " + "  Time: " + hr + ":" + min + '\n'
                                    acc_member[1] = 1

                except:
                    pass
    if int(acc_member[1]) == 0:
        string = string + str(acc_member[0]) + ":   khong co mat ? "+ '\n'
    else:
        acc_member[1] = 0
    return string

def get_checkout_member(acc_member, string, file_log):
    y = datetime.datetime.now().strftime("%Y")
    m = datetime.datetime.now().strftime("%m")
    d = datetime.datetime.now().strftime("%d")
    H = datetime.datetime.now().strftime("%H")
    M = datetime.datetime.now().strftime("%M")
    S = datetime.datetime.now().strftime("%S")
    sttr = ''
    with open(file_log) as f:
            for line in f:
                try:
                    if str(line.split(' ')[0]) == 'XXX':
                        mon = line.split('-')[1]
                        day = line.split('-')[2].split(' ')[0]
                        hr = line.split(':')[0].split(' ')[2]
                        min = line.split(':')[1]
                        if m == mon and d == day:
                            ACC = line.split(' ')[6].split('\n')[0]
                            if ACC == str(acc_member[0]):
                                sttr = str(acc_member[0]) + ":  Checkin lan sau cung -> " + " Time : " + hr + ":" + min + '\n'
                                acc_member[1] = 1

                except:
                    pass
    if int(acc_member[1]) == 0:
        sttr = str(acc_member[0]) + ":  khong co mat ? " + '\n'
    else:
        acc_member[1] = 0
    return string + sttr

def main_():
    path_excel = "./DS.xlsx"
    DS_Member1, DS = get_ds(path_excel)
    subject_tracking_mem = "Tracking time in/out member :" + str(datetime.datetime.now().strftime("%Y-%m-%d"))
    path_file_log = "./ares.log"
    message_plus =""
    message_in = "Tracking checkin member :" + str(datetime.datetime.now().strftime("%Y-%m-%d")) + "\n\n"
    message_out = "Tracking checkout member :" + str(datetime.datetime.now().strftime("%Y-%m-%d")) + "\n\n"
    for name in DS_Member1:
        message_in = get_checkin_member(name, message_in, path_file_log)
        message_out = get_checkout_member(name, message_out, path_file_log)
        checkin_checkout(name, path_file_log, path_excel)
    message_plus = message_in +'\n\n'+'------------------------------------------------'+'\n\n'+ message_out
    send_email(message_plus, subject_tracking_mem, your_host.get(), your_port.get(), MY_ADDRESS.get(),
                    PASSWORD.get(), mail_to.get(), mail_cc.get())

def exit():
    root.quit()
if __name__ == '__main__':
    root = tk.Tk()
    root.title('VBPO')
    root.geometry('800x400')

    # =======Set up mail ===========================#
    MY_ADDRESS = tk.Entry(root)
    label_mail_login = tk.Label(root, text='Login Email: ')
    label_mail_login.place(relx=0.05, rely=0.16, anchor='c')
    MY_ADDRESS.place(relx=0.1, rely=0.15, height=20, width=200)
    MY_ADDRESS.insert(0, "")

    PASSWORD = tk.Entry(root,show="*")
    label_pass_login = tk.Label(root, text='Password: ')
    label_pass_login.place(relx=0.05, rely=0.22, anchor='c')
    PASSWORD.place(relx=0.1, rely=0.21, height=20, width=200)
    PASSWORD.insert(0, "")

    your_host = tk.Entry(root)
    label_your_host = tk.Label(root, text='Your Host: ')
    label_your_host.place(relx=0.05, rely=0.28, anchor='c')
    your_host.place(relx=0.1, rely=0.27, height=20, width=200)
    your_host.insert(0, 'smtp.gmail.com')

    your_port = tk.Entry(root)
    label_your_port = tk.Label(root, text='Your Port: ')
    label_your_port.place(relx=0.05, rely=0.34, anchor='c')
    your_port.place(relx=0.1, rely=0.33, height=20, width=50)
    your_port.insert(0, 587)

    # ======= Link Mail To ===========================#
    mail_to = tk.Entry(root)
    label_mail_to = tk.Label(root, text='email To: ')
    label_mail_to.place(relx=0.05, rely=0.4, anchor='c')
    mail_to.place(relx=0.1, rely=0.39, height=20, width=500)
    mail_to.insert(0, "")

    # ======= Link Mail CC ===========================#
    mail_cc = tk.Entry(root)
    label_mail_cc = tk.Label(root, text='email CC: ')
    label_mail_cc.place(relx=0.05, rely=0.46, anchor='c')
    mail_cc.place(relx=0.1, rely=0.45, height=20, width=500)
    mail_cc.insert(0, "")

    # ======= Button Main ===========================#
    btn = tk.Button(root, text='SEND', command=main_)
    btn.place(relx=.1, rely=.05,height=30, width=60, anchor='c')

    # ======= Button Exit ===========================#
    exitbt = tk.Button(root, text='Exit', command=exit)
    exitbt.place(relx=.9, rely=.05, height=30, width=60, anchor='c')

    # kick off the GUI
    root.mainloop()