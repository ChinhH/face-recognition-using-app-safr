import os
import time
import datetime
import win32com.client as win32
# print(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
m = datetime.datetime.now().strftime("%m")
d = datetime.datetime.now().strftime("%d")
H = datetime.datetime.now().strftime("%H")
M = datetime.datetime.now().strftime("%M")
S = datetime.datetime.now().strftime("%S")
# print(d)
# print(m)
# print(H)
# print(M)
path_in = "./ares.log"
path_save = ""
hr_check = 8
min_check = 28
# ---------------------
# print(string)
list = 'TrungPM,ChinhH,PhungPD,HuyPT,TuTu,VietHN,HaiPM,HuyTran,HaHT,HuongNTT'
DS = list.split(',')
print(len(DS))
DS_Member = []
for i in range(len(DS)):
    DS_Member.append([DS[i],0])

def get_checkin_member(acc_member, string, file_log):
    with open(file_log) as f:
            for line in f:
                try:
                    if str(line.split(' ')[0]) == 'XXX':
                        mon = line.split('-')[1]
                        day = line.split('-')[2].split(' ')[0]
                        hr = line.split(':')[0].split(' ')[2]
                        min = line.split(':')[1]
                        # print('acc_member[1]',acc_member[1])
                        if m == mon and d == day:
                            ACC = line.split(' ')[6]
                            if int(hr) < hr_check or (int(hr) == hr_check and int(min) <= min_check):
                                if ACC == str(acc_member[0]):
                                    if int(acc_member[1]) == 0:
                                        string = string + str(acc_member[0])+": checkin success ! " + "Time: " + hr + ":" + min + '\n'
                                        acc_member[1] = 1
                            else:
                                if ACC == str(acc_member[0]):
                                    if int(acc_member[1]) == 0:
                                        string = string + str(acc_member[0]) + ": checkin Too Late ! " + "Time: " + hr + ":" + min + '\n'
                                        acc_member[1] = 1
                except:
                    pass
    if int(acc_member[1]) == 0:
        string = string + str(acc_member[0]) + ": absence ! " + '\n'
    else:
        acc_member[1] = 0
    return string
    
def Tracking(file_log): 
    string = "Tracking time in/out member DRI " + str(datetime.datetime.now().strftime("%Y-%m-%d")) + "\n"
    # danh sach member ---
    ChinhH = 0
    PhungPD = 0
    HuyPT = 0
    TuTu = 0
    HaiPM = 0
    VietHN = 0
    HaHT = 0
    HuongNTT = 0
    TrungPM = 0
    # ---------------------
    with open(file_log) as f:
            for line in f:
                try:
                    if str(line.split(' ')[0]) == 'XXX':
                        mon = line.split('-')[1]
                        day = line.split('-')[2].split(' ')[0]
                        hr = line.split(':')[0].split(' ')[2]
                        min = line.split(':')[1]
                        if m == mon and d == day:
                            # print(line)
                            ACC = line.split(' ')[6]
                            if int(hr) < hr_check or (int(hr) == hr_check and int(min) <= min_check):
                                if ACC == 'ChinhH':
                                    if ChinhH == 0:
                                        string = string + "ChinhH____: checkin success ! " + "Time: " + hr + ":" + min + '\n'
                                        ChinhH = 1
                                elif ACC == 'PhungPD':
                                    if PhungPD == 0:
                                        string = string + "PhungPD___: checkin success ! " + "Time: " + hr + ":" + min + '\n'
                                        PhungPD = 1
                                elif ACC == 'HuyPT':
                                    if HuyPT == 0:
                                        string = string + "HuyPT_____: checkin success ! " + "Time: " + hr + ":" + min + '\n'
                                        HuyPT = 1
                                elif ACC == 'TuTu':
                                    if TuTu == 0:
                                        string = string + "TuTu______: checkin success ! " + "Time: " + hr + ":" + min + '\n'
                                        TuTu = 1
                                elif ACC == 'HaiPM':
                                    if HaiPM == 0:
                                        string = string + "HaiPM_____: checkin success ! " + "Time: " + hr + ":" + min + '\n'
                                        HaiPM = 1
                                elif ACC == 'VietHN':
                                    if VietHN == 0:
                                        string = string + "VietHN____: checkin success ! " + "Time: " + hr + ":" + min + '\n'
                                        VietHN = 1
                                elif ACC == 'HaHT':
                                    if HaHT == 0:
                                        string = string + "HaHT______: checkin success ! " + "Time: " + hr + ":" + min + '\n'
                                        HaHT = 1
                                elif ACC == 'HuongNTT':
                                    if HuongNTT == 0:
                                        string = string + "HuongNTT__: checkin success ! " + "Time: " + hr + ":" + min + '\n'
                                        HuongNTT = 1
                                elif ACC == 'TrungPM':
                                    if TrungPM == 0:
                                        string = string + "TrungPM___: checkin success ! " + "Time: " + hr + ":" + min + '\n'
                                        TrungPM = 1
                            else:
                                if ACC == 'ChinhH':
                                    if ChinhH == 0:
                                        string = string + "ChinhH____: checkin Too Late ! " + "Time: " + hr + ":" + min + '\n'
                                        ChinhH = 1
                                elif ACC == 'PhungPD':
                                    if PhungPD == 0:
                                        string = string + "PhungPD___: checkin Too Late ! " + "Time: " + hr + ":" + min + '\n'
                                        PhungPD = 1
                                elif ACC == 'HuyPT':
                                    if HuyPT == 0:
                                        string = string + "HuyPT_____: checkin Too Late ! " + "Time: " + hr + ":" + min + '\n'
                                        HuyPT = 1
                                elif ACC == 'TuTu':
                                    if TuTu == 0:
                                        string = string + "TuTu______: checkin Too Late ! " + "Time: " + hr + ":" + min + '\n'
                                        TuTu = 1
                                elif ACC == 'HaiPM':
                                    if HaiPM == 0:
                                        string = string + "HaiPM_____: checkin Too Late ! " + "Time: " + hr + ":" + min + '\n'
                                        HaiPM = 1
                                elif ACC == 'VietHN':
                                    if VietHN == 0:
                                        string = string + "VietHN____: checkin Too Late ! " + "Time: " + hr + ":" + min + '\n'
                                        VietHN = 1
                                elif ACC == 'HaHT':
                                    if HaHT == 0:
                                        string = string + "HaHT______: checkin Too Late ! " + "Time: " + hr + ":" + min + '\n'
                                        HaHT = 1
                                elif ACC == 'HuongNTT':
                                    if HuongNTT == 0:
                                        string = string + "HuongNTT__: checkin Too Late ! " + "Time: " + hr + ":" + min + '\n'
                                        HuongNTT = 1
                                elif ACC == 'TrungPM':
                                    if TrungPM == 0:
                                        string = string + "TrungPM___: checkin Too Late ! " + "Time: " + hr + ":" + min + '\n'
                                        TrungPM = 1
                except:
                    pass
    if ChinhH == 0:
        string = string + "ChinhH____: absence ! " + '\n'
    if PhungPD == 0:
        string = string + "PhungPD___: absence ! " + '\n'
    if HuyPT == 0:
        string = string + "HuyPT_____: absence ! " + '\n'
    if TuTu == 0:
        string = string + "TuTu______: absence ! " + '\n'
    if HaiPM == 0:
        string = string + "HaiPM_____: absence ! " + '\n'
    if VietHN == 0:
        string = string + "VietHN____: absence ! " + '\n'
    if HaHT == 0:
        string = string + "HaHT______: absence ! " + '\n'
    if HuongNTT == 0:
        string = string + "HuongNTT__: absence ! " + '\n'
    if TrungPM == 0:
        string = string + "TrungPM___: absence ! " + '\n'
    return string
# print(string)
#---------sign-------------------------------------
sign = "\n"+"\n"+"-------------------------------------" + "\n" + "HUYNH CHINH" + "\n" + "V.B.P.O Joint Stock Company" + "\n" +'12F Danang Software Park, 02 Quang Trung St., Danang City, Vietnam'+'\n'+'Tel: 084-2363-6510087'+'\n'+'Website: www.vbpo.com.vn'+'\n'+'IMPORTANT NOTICE'+'\n'+'The information in this e-mail and any attached files is CONFIDENTIAL and may be legally privileged or prohibited from disclosure and unauthorized use. The views of the author may not necessarily reflect those of the Company. It is intended solely for the addressee, or the employee or agent responsible for delivering such materials to the addressee. If you have received this message in error please return it to the sender then delete the email and destroy any copies of it. If you are not the intended recipient, any form of reproduction, dissemination, copying, disclosure, modification, distribution and/or publication or any action taken or omitted to be taken in reliance upon this message or its attachments is prohibited and may be unlawful. At present the integrity of e-mail across the Internet cannot be guaranteed and messages sent via this medium are potentially at risk. All liability is excluded to the extent permitted by law for any claims arising as a result of the use of this medium to transmit information by or to V.B.P.O JSC'+'\n' +'Please consider the environment before printing this email'+'\n'
# print(sign)

def send_Email_Outlook(message,sign):
    outlook = win32.Dispatch('outlook.application')
    mail = outlook.CreateItem(0)
    mail.To = 'huynh.chinh@vbpo.com.vn'
    mail.CC = 'huynh.chinh@vbpo.com.vn'
    mail.Subject = 'Tracking time in/out member DRI'
    mail.Body = message + sign
    mail.Send()
    print('Done Send email')

def main():
    # ---- set time check and send email ------
    day_send = 57
    hr_send = '14'
    min_send = '11'
    # -----------------------------------------
    
    while True:
        time.sleep(0.1)
        m = datetime.datetime.now().strftime("%m")
        d = datetime.datetime.now().strftime("%d")
        H = datetime.datetime.now().strftime("%H")
        M = datetime.datetime.now().strftime("%M")
        S = datetime.datetime.now().strftime("%S")
        # if str(H) == str(hr_send) and str(M) == str(min_send):
        # if str(day_send) == str(d): # send email day
        if str(day_send) == str(M):  # send email day
            message_in = "Tracking checkin member DRI " + str(datetime.datetime.now().strftime("%Y-%m-%d")) + "\n\n"
            for name in DS_Member:
                message_in = get_checkin_member(name, message_in, path_in)
            message_out = "Tracking checkin member DRI " + str(datetime.datetime.now().strftime("%Y-%m-%d")) + "\n\n"
            # for name in DS_Member:
            #     message_out = get_checkout_member(name, message_out, path_in)
            message = message_in + "\n\n"+ message_out
            print(message+sign)
            # send_Email_Outlook(message,sign)
            day_send+=1

if __name__ == '__main__':
    main()
    # while True:
        # m = datetime.datetime.now().strftime("%m")
        # d = datetime.datetime.now().strftime("%d")
        # H = datetime.datetime.now().strftime("%H")
        # M = datetime.datetime.now().strftime("%M")
        # S = datetime.datetime.now().strftime("%S")
        # time.sleep(0.01)
        # if str(H) == str(hr_send) and str(M) == str(min_send):
            # if str(day_send) == str(d): # send email day
            # if str(day_send) == str(H): # send email hr
                # print('va0')
                # message = Tracking(path_in)
                # print(message+sign)
                # send_Email_Outlook(message,sign)
                # day_send+=1
