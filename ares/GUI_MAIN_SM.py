# For GUI
import cv2,os
import tkinter as tk
import win32com.client as win32
from tkinter import filedialog
from tkinter.scrolledtext import ScrolledText
import tkinter.font as tkFont
from gtts import gTTS

import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from playsound import playsound
from PIL import Image, ImageTk
from openpyxl import load_workbook
from pandas import ExcelWriter
import pandas as pd
import datetime
import time
# y = datetime.datetime.now().strftime("%Y")
# m = datetime.datetime.now().strftime("%m")
# d = datetime.datetime.now().strftime("%d")
# H = datetime.datetime.now().strftime("%H")
# M = datetime.datetime.now().strftime("%M")
# S = datetime.datetime.now().strftime("%S")
import easygui
from tkinter import font
# appHighlightFont = font.Font(family='Helvetica', size=14, weight='bold')

status = True
visible = True
check_sendmail_checkin = True
check_sendmail_checkout = True
check_export_file_excel = True
st_save = False

def get_ds(path_excel):
    DANHSACH = pd.read_excel(path_excel, sheet_name='base', dtype=object)
    DS_Member1 = []
    DS = []
    for i in range(len(DANHSACH['ACC'])):
        if str(DANHSACH['ACC'][i]) != 'nan':
            DS.append(DANHSACH['ACC'][i])
            DS_Member1.append([DANHSACH['ACC'][i], 0])
    return DS_Member1,DS

def send_Email_Outlook(message,email_subject,email_to,email_cc):
    outlook = win32.Dispatch('outlook.application')
    mail = outlook.CreateItem(0)
    mail.To = email_to
    mail.CC = email_cc
    mail.Subject = str(email_subject)
    mail.Body = message
    mail.Send()

def main_send_email(message,subject,your_host,your_port,MY_ADDRESS,PASSWORD,To_email, CC_email):
    # set up the SMTP server
    s = smtplib.SMTP(host=your_host, port=your_port)
    try:
        s.starttls()
        s.ehlo()
    except: pass
    s.login(MY_ADDRESS, PASSWORD)
    # For each contact, send the email:
    # for name, email in zip(names, emails):
    msg = MIMEMultipart()  # create a message
    # add in the actual person name to the message template
    # message = message_template.substitute(PERSON_NAME=name.title())
    # Prints out the message body for our sake

    # setup the parameters of the message
    msg['From'] = MY_ADDRESS
    msg['To'] = To_email
    msg['CC'] = CC_email
    msg['Subject'] = subject

    # add in the message body
    msg.attach(MIMEText(message, 'plain'))

    # send the message via the server set up earlier.
    s.send_message(msg)
    del msg

    # Terminate the SMTP session and close the connection
    s.quit()

def fill_file_excel(filexl,ACC,Checkin,Checkout):
    dfOutput1 = pd.read_excel(filexl,  sheet_name='EXPORT',dtype=object)
    for i in range(0, 2000):
        if str(dfOutput1['ACC'][i]) == 'nan':
            dfOutput1.iat[i, 3] = ACC
            dfOutput1.iat[i, 1] = Checkin
            dfOutput1.iat[i, 11] = Checkout
            break
    for i in range(0, 2000):
        dfOutput1.iat[i, 2] = ('=IF(D{}="",IF(B{}="","","NGUOI_LA"),VLOOKUP(D{},base!$B$2:$J$101,2,0))').format(i + 2,
                                                                                                                i + 2,
                                                                                                                i + 2)
        dfOutput1.iat[i, 4] = ('=IF(D{}="","",VLOOKUP(D{},base!$B$2:$J$101,3,0))').format(i + 2, i + 2)
        dfOutput1.iat[i, 5] = ('=IF(C{}="NGUOI_LA","NO",IF(D{}="","",VLOOKUP(D{},base!$B$2:$J$101,4,0)))').format(i + 2,
                                                                                                                  i + 2,
                                                                                                                  i + 2)
        dfOutput1.iat[i, 6] = ('=IF(C{}="NGUOI_LA","NO",IF(D{}="","",VLOOKUP(D{},base!$B$2:$J$101,5,0)))').format(i + 2,
                                                                                                                  i + 2,
                                                                                                                  i + 2)
        dfOutput1.iat[i, 7] = ('=IF(C{}="NGUOI_LA","NO",IF(D{}="","",VLOOKUP(D{},base!$B$2:$J$101,6,0)))').format(i + 2,
                                                                                                                  i + 2,
                                                                                                                  i + 2)
        dfOutput1.iat[i, 8] = ('=IF(D{}="","",VLOOKUP(D{},base!$B$2:$J$101,7,0))').format(i + 2, i + 2)
        dfOutput1.iat[i, 9] = ('=IF(C{}="NGUOI_LA",O{},IF(D{}="","",VLOOKUP(D{},base!$B$2:$J$101,8,0)))').format(i + 2,
                                                                                                                 i + 2,
                                                                                                                 i + 2,
                                                                                                                 i + 2)
        dfOutput1.iat[i, 10] = ('=IF(C{}="NGUOI_LA",P{},IF(D{}="","",VLOOKUP(D{},base!$B$2:$J$101,9,0)))').format(i + 2,
                                                                                                                  i + 2,
                                                                                                                  i + 2,
                                                                                                                  i + 2)
    book = load_workbook(filexl)
    writer = ExcelWriter(filexl, engine='openpyxl')
    writer.book = book
    writer.sheets = dict((ws.title, ws) for ws in book.worksheets)
    dfOutput1.to_excel(writer, 'EXPORT', index=False, startrow=1, header=False)
    writer.save()

def checkin_checkout(acc_member,file_log,path_exl):
    y = datetime.datetime.now().strftime("%Y")
    m = datetime.datetime.now().strftime("%m")
    d = datetime.datetime.now().strftime("%d")
    H = datetime.datetime.now().strftime("%H")
    M = datetime.datetime.now().strftime("%M")
    S = datetime.datetime.now().strftime("%S")
    with open(file_log) as f:
        time_in = ""
        time_out =""
        for line in f:
            try:
                if str(line.split(' ')[0]) == 'XXX':
                    mon = line.split('-')[1]
                    day = line.split('-')[2].split(' ')[0]
                    hr = line.split(':')[0].split(' ')[2]
                    min = line.split(':')[1]
                    se = line.split(':')[2].split(' ')[0]
                    if m == mon and d == day:
                        ACC = line.split(' ')[7].split('\n')[0]
                        if ACC == str(acc_member[0]):
                            if int(acc_member[1]) == 0:
                                time_in = y + '-' + mon+'-' + day + " "+hr + ":" + min + ":"+ se
                                acc_member[1] = 1
                            time_out = y + '-' + mon+'-' + day + " "+hr + ":" + min + ":"+ se
                            acc_member[1] = 1
            except:
                pass
    if int(acc_member[1]) == 1:
        acc_member[1] = 0
        fill_file_excel(path_exl,str(acc_member[0]),time_in,time_out)

def get_checkin_member(acc_member, string, file_log, hr_check, min_check):
    y = datetime.datetime.now().strftime("%Y")
    m = datetime.datetime.now().strftime("%m")
    d = datetime.datetime.now().strftime("%d")
    H = datetime.datetime.now().strftime("%H")
    M = datetime.datetime.now().strftime("%M")
    S = datetime.datetime.now().strftime("%S")
    with open(file_log) as f:
            for line in f:
                try:
                    if str(line.split(' ')[0]) == 'XXX':
                        mon = line.split('-')[1]
                        day = line.split('-')[2].split(' ')[0]
                        hr = line.split(':')[0].split(' ')[2]
                        min = line.split(':')[1]
                        if m == mon and d == day:
                            ACC = line.split(' ')[7].split('\n')[0]
                            if int(hr) < hr_check or (int(hr) == hr_check and int(min) <= min_check):
                                if ACC == str(acc_member[0]):
                                    if int(acc_member[1]) == 0:
                                        string = string  + str(acc_member[0])+": di dung gio ! " + "  Time: " + hr + ":" + min + '\n'
                                        acc_member[1] = 1
                            else:
                                if ACC == str(acc_member[0]):
                                    if int(acc_member[1]) == 0:
                                        string = string + str(acc_member[0]) + ": di qua tre ? " + "  Time: " + hr + ":" + min+ '\n'
                                        acc_member[1] = 1

                except:
                    pass
    if int(acc_member[1]) == 0:
        string = string + str(acc_member[0]) + ": khong co mat ??? "+ '\n'
    else:
        acc_member[1] = 0
    return string

def get_checkout_member(acc_member, string, file_log, hr_check, min_check):
    y = datetime.datetime.now().strftime("%Y")
    m = datetime.datetime.now().strftime("%m")
    d = datetime.datetime.now().strftime("%d")
    H = datetime.datetime.now().strftime("%H")
    M = datetime.datetime.now().strftime("%M")
    S = datetime.datetime.now().strftime("%S")
    sttr = ''
    with open(file_log) as f:
            for line in f:
                try:
                    if str(line.split(' ')[0]) == 'XXX':
                        mon = line.split('-')[1]
                        day = line.split('-')[2].split(' ')[0]
                        hr = line.split(':')[0].split(' ')[2]
                        min = line.split(':')[1]
                        if m == mon and d == day:
                            ACC = line.split(' ')[7].split('\n')[0]
                            if ACC == str(acc_member[0]):
                                if int(hr) < hr_check or (int(hr) == hr_check and int(min) < min_check):
                                    sttr = str(acc_member[0]) + ": Ve som ???" + "__Time : " + hr + ":" + min + '\n'
                                    acc_member[1] = 1
                                elif int(hr) == hr_check and int(min) >= min_check:
                                    sttr = str(acc_member[0]) + ": Ve dung gio " + "__Time : " + hr + ":" + min + '\n'
                                    acc_member[1] = 1
                                elif int(hr) > hr_check and int(hr) < 23:
                                    sttr = str(acc_member[0]) + ": OverTime => Ve tre " + "__Time : " + hr + ":" + min + '\n'
                                    acc_member[1] = 1
                                elif int(hr) == 23:
                                    sttr = str(acc_member[0]) + ": OverNight => Ve tre " + "__Time : " + hr + ":" + min + '\n'
                                    acc_member[1] = 1
                except:
                    pass
    if int(acc_member[1]) == 0:
        sttr = str(acc_member[0]) + ": khong co mat ??? " + '\n'
    else:
        acc_member[1] = 0
    return string + sttr

def save_image():
    global st_save
    st_save = True

def call_camera():
    global st_save
    if cap.isOpened():
        try:
            name = datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S") + "_frame.jpg"
            ret, frame = cap.read()
            if frame is not None:
                placeholder = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
                # placeholder = cv2.resize(placeholder,(800,600))
                placeholder = Image.fromarray(placeholder)
                placeholder = ImageTk.PhotoImage(placeholder)
                panelimg.configure(image=placeholder)
                panelimg.image = placeholder
                if st_save == True:
                    # print('11111')
                    cv2.imwrite('D:/VBPO/image/'+ name, frame)
                    st_save = False
        except:pass
    # print('OK')
    root.after(1, call_camera)

def _complex_():
    global check_sendmail_checkin
    global check_sendmail_checkout
    global check_export_file_excel
    path_excel = file_excel.get()
    DS_Member1, DS = get_ds(path_excel)
    subject_tracking_mem = "Tracking time in/out member :" + str(datetime.datetime.now().strftime("%Y-%m-%d"))
    subject_tracking_nguoila = "Tracking co nguoi la xuat hien :" + str(datetime.datetime.now().strftime("%Y-%m-%d"))
    # -----------------------------------------
    # while True:
    path_file_log = "./ares.log"
    m = datetime.datetime.now().strftime("%m")
    d = datetime.datetime.now().strftime("%d")
    H = datetime.datetime.now().strftime("%H")
    M = datetime.datetime.now().strftime("%M")
    S = datetime.datetime.now().strftime("%S")

    # ===== send email checkin member ===========
    if int(H) == int(h_sendEmail_checkin.get()) and int(M) == int(min_sendEmail_checkin.get()):
        if check_sendmail_checkin == True:
            message_in = "Tracking checkin member :" + str(datetime.datetime.now().strftime("%Y-%m-%d")) + "\n\n"
            for name in DS_Member1:
                message_in = get_checkin_member(name, message_in, path_file_log, hr_check=int(h_checkin.get()),min_check=int(min_checkin.get()))
            # send_Email_Outlook(message_in,subject_tracking_mem,mail_to.get(),mail_cc.get())
            main_send_email(message_in,subject_tracking_mem,your_host.get(),your_port.get(),MY_ADDRESS.get(),PASSWORD.get(), mail_to.get(), mail_cc.get())
            # print(message_in)
            check_sendmail_checkin = False
    else: check_sendmail_checkin = True
    # ===== send email checkout member ===========
    if int(H) == int(h_sendEmail_checkout.get()) and int(M) == int(min_sendEmail_checkout.get()):
        if check_sendmail_checkout == True:
            message_out = "Tracking checkout member :" + str(datetime.datetime.now().strftime("%Y-%m-%d")) + "\n\n"
            for name in DS_Member1:
                message_out = get_checkout_member(name, message_out, path_file_log, hr_check=int(h_checkout.get()),min_check=int(min_checkout.get()))
            # send_Email_Outlook(message_out,subject_tracking_mem,mail_to.get(),mail_cc.get())
            main_send_email(message_out, subject_tracking_mem, your_host.get(), your_port.get(), MY_ADDRESS.get(),
                            PASSWORD.get(), mail_to.get(), mail_cc.get())
            # print(message_out)
            check_sendmail_checkout = False
    else: check_sendmail_checkout = True

    # ===== export file excel ===========
    if int(H) == int(h_exportExcel.get()) and int(M) == int(min_exportExcel.get()):
        if check_export_file_excel == True:
            for name in DS_Member1:
                checkin_checkout(name, path_file_log,path_excel)
            check_export_file_excel = False
    else: check_export_file_excel = True

def insert_value():
    insert0 = h_checkin.get()
    insert1 = min_checkin.get()
    insert2 = h_checkout.get()
    insert3 = min_checkout.get()
    insert4 = h_sendEmail_checkin.get()
    insert5 = min_sendEmail_checkin.get()
    insert6 = h_sendEmail_checkout.get()
    insert7 = min_sendEmail_checkout.get()
    insert8 = h_exportExcel.get()
    insert9 = min_exportExcel.get()
    insert10 = file_excel.get()
    insert11 = MY_ADDRESS.get()
    insert12 = PASSWORD.get()
    insert13 = your_host.get()
    insert14 = your_port.get()
    insert15 = mail_to.get()
    insert16 = mail_cc.get()
    string = insert0 + '|' + insert1 + '|'+ insert2 + '|' + insert3 + '|' + insert4 + '|' + insert5 + '|' +  insert6 + '|' + insert7 + '|'+ insert8 + '|' + insert9 + '|' + insert10 + '|' + insert11 + '|' + insert12 + '|' + insert13 + '|' + insert14 + '|' + insert15 + '|' +  insert16 + '|'
    with open('./insert_config.txt', 'w') as txt_insert:
        txt_insert.write(string)
        txt_insert.close()

def main():
    global status
    global visible
    insert_value()
    if visible == True:
        btnstop.place(relx=.1, rely=.05, height=30, width=60, anchor='c')
        btnstop.visible = True
        btn.place_forget()
        visible = False
        status = True
    if status == True:
        # control_voice()
        _complex_()
        # control_voice()
        # print(status)
        root.after(10, main)
    else:
        visible = True

def btn_stop():
    global status
    global visible
    btn.place(relx=.1, rely=.05, height=30, width=60, anchor='c')
    btn.visible = True
    btnstop.place_forget()
    status = False
    visible = False

def exit():
    insert_value()
    root.destroy()

def creat_voice(mytext,language,path_save):
    myobj = gTTS(text=mytext, lang=language, slow=False)
    myobj.save(path_save)

def control_voice():
    mytext = 'xin chào'
    path_save = "D:/VBPO/voice/"
    language = 'vi'
    # language = 'en'
    try:
        with open("./voice.txt",'r', encoding='utf-8') as text:
            content = text.read()
            text.close()
            filexl = "D:/VBPO/DS.xlsx"
            dfOutput1 = pd.read_excel(filexl, sheet_name='RING', dtype=object)
            tus = 1
            for i in range(0, 20000):
                if str(dfOutput1['name'][i]).split(':')[0] == content.split(":")[0]:
                    tus = 0
                    break
            # print(tus)
            if tus == 1:
                mp3 = content.split('-')[0] + ".mp3"
                # print(path_save + mp3)
                # creat_voice(texxt, language, path_save)
                playsound(path_save + mp3)
                for i in range(0, 20000):
                    if str(dfOutput1['name'][i]) == 'nan':
                        dfOutput1.iat[i, 1] = content.split('|')[0]
                        break
                book = load_workbook(filexl)
                writer = ExcelWriter(filexl, engine='openpyxl')
                writer.book = book
                writer.sheets = dict((ws.title, ws) for ws in book.worksheets)
                dfOutput1.to_excel(writer, 'RING', index=False, startrow=1, header=False)
                writer.save()
    except: pass
    # root.after(1000,control_voice)
def add_mem():
    root2 = tk.Tk()
    root2.title('Tao thong tin nhan vien moi')
    root2.geometry('600x300')
    customFont = tkFont.Font(family='Timenewromand', size=17)
    def thoat():
        root2.destroy()

    y1=0.07
    x1=0.1
    full_name = tk.Entry(root2, font=customFont)
    full_name.place(relx=x1+0.08, rely=y1, height=20, width=200)

    label_name = tk.Label(root2, text="Họ và Tên(*):")
    label_name.place(relx=x1,rely=y1+0.03, anchor='c')

    acc = tk.Entry(root2)
    acc.place(relx=x1+0.08, rely=y1+0.1, height=20, width=200)
    label_acc = tk.Label(root2, text="ACC (*):")
    label_acc.place(relx=x1, rely=y1+0.13, anchor='c')

    sdt = tk.Entry(root2)
    sdt.place(relx=x1 + 0.08, rely=y1 + 0.2, height=20, width=200)
    label_sdt = tk.Label(root2, text="Số điện thoại(*):")
    label_sdt.place(relx=x1, rely=y1 + 0.23, anchor='c')

    namsinh = tk.Entry(root2)
    namsinh.place(relx=x1 + 0.08, rely=y1 + 0.3, height=20, width=100)
    label_namsinh = tk.Label(root2, text="Năm sinh(*):")
    label_namsinh.place(relx=x1, rely=y1 + 0.33, anchor='c')

    gioitinh = tk.Entry(root2)
    gioitinh.place(relx=x1 + 0.08, rely=y1 + 0.4, height=20, width=100)
    label_gioitinh = tk.Label(root2, text="Giới Tính(*):")
    label_gioitinh.place(relx=x1, rely=y1 + 0.43, anchor='c')

    chucvu = tk.Entry(root2)
    chucvu.place(relx=x1 + 0.08, rely=y1 + 0.5, height=20, width=200)
    label_chucvu = tk.Label(root2, text="Chức vụ:")
    label_chucvu.place(relx=x1, rely=y1 + 0.53, anchor='c')

    ttthem = tk.Entry(root2)
    ttthem.place(relx=x1 + 0.08, rely=y1 + 0.6, height=20, width=200)
    label_ttthem = tk.Label(root2, text="Thông tin thêm:")
    label_ttthem.place(relx=x1, rely=y1 + 0.63, anchor='c')

    def save_mem_excel():
        file_ex = file_excel.get()
        contentfile = pd.read_excel(file_ex, sheet_name='base', dtype=object)
        if full_name.get() != "":
            if acc.get() !="":
                if sdt.get() !="" and len(sdt.get()) > 9 and len(sdt.get()) < 12:
                    if namsinh.get() != ""  and len(namsinh.get()) == 4:
                        if gioitinh.get() != "" and (gioitinh.get() == "nam" or gioitinh.get() == "Nam" or gioitinh.get() == "NAM" or gioitinh.get() == "Nữ"or gioitinh.get() == "Nu" or gioitinh.get() == "NỮ" or gioitinh.get() == "NU" or gioitinh.get() == "nu"or gioitinh.get() == "nữ"):
                            try:
                                if int(namsinh.get()) > 0 and int(sdt.get()) > 0:
                                    for i in range(0, 2000000):
                                        if str(contentfile['Tên'][i]) == str(full_name.get()):
                                            break
                                        elif str(contentfile['Tên'][i]) == 'nan':
                                            contentfile.iat[i, 1] = acc.get()
                                            contentfile.iat[i, 2] = full_name.get()
                                            contentfile.iat[i, 3] = sdt.get()
                                            contentfile.iat[i, 4] = chucvu.get()
                                            contentfile.iat[i, 7] = ttthem.get()
                                            contentfile.iat[i, 8] = namsinh.get()
                                            contentfile.iat[i, 9] = gioitinh.get()
                                            break
                                    try:
                                        book = load_workbook(file_ex)
                                        writer = ExcelWriter(file_ex, engine='openpyxl')
                                        writer.book = book
                                        writer.sheets = dict((ws.title, ws) for ws in book.worksheets)
                                        contentfile.to_excel(writer, 'base', index=False, startrow=1, header=False)
                                        writer.save()
                                        root2.destroy()
                                    except:
                                        easygui.msgbox(
                                            "Bạn cần đảm bảo tắt các file excel trước khi chạy chương trình!",
                                            title="cảnh báo")
                            except:
                                easygui.msgbox("Bạn cần kiểm tra lại thông tin năm sinh hoặc số điện thoại đang không đúng!", title="canh bao")
                        else:
                            easygui.msgbox("Bạn cần thêm thông tin giới tính!", title="cảnh báo")
                    else:
                        easygui.msgbox("Bạn cần thêm thông tin năm sinh!", title="cảnh báo")
                else:
                    easygui.msgbox("Bạn cần thêm thông tin số điện thoại!", title="cảnh báo")
            else:
                easygui.msgbox("Bạn cần thêm thông tin ACC!", title="cảnh báo")
        else:
            easygui.msgbox("Bạn cần thêm thông tin họ và tên!", title="cảnh báo")
        

    OK_save = tk.Button(root2, text='Xong', command=save_mem_excel)
    OK_save.place(relx=.2, rely=.83, height=30, width=100, anchor='c')

    Cancel = tk.Button(root2, text='Hủy', command=thoat)
    Cancel.place(relx=.5, rely=.83, height=30, width=100, anchor='c')
def load_logo(image):
    frame = cv2.imread(image)
    placeholder = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
    placeholder = cv2.resize(placeholder,(200,200))
    placeholder = Image.fromarray(placeholder)
    placeholder = ImageTk.PhotoImage(placeholder)
    panelimg.configure(image=placeholder)
    panelimg.image = placeholder
if __name__ == '__main__':
    root = tk.Tk()
    root.title('Phần mềm quản lý điểm danh')
    root.geometry('800x600')
    with open('./insert_config.txt', 'r') as txt_insert:
        insert_content = txt_insert.read()
        txt_insert.close()
    # print(insert_content.split('|')[0])

    # ======= Link camera ===========================#
    link_camera = tk.Entry(root)
    # label_link_camera = tk.Label(root, text='Link_camera: ')
    # label_link_camera.place(relx=0.07, rely=0.95, anchor='c')
    # link_camera.place(relx=0.12, rely=0.94, height=20, width=300)
    link_camera.insert(0, 'rtsp://admin:abcd1234@192.168.0.87:554/cam/realmonitor?channel=1&subtype=1&unicast=true&proto=Onvif')


    # ======= Check In ==============================#
    h_checkin = tk.Entry(root,font=("Times New Roman", 12),foreground="red")
    min_checkin = tk.Entry(root,font=("Times New Roman", 12),foreground="red")

    Xx = 0.1
    Yy = 0.42

    Time_checkin = tk.Label(root, text='Thời gian vào: ',font=("Times New Roman", 12, "bold"),foreground="blue")
    # text.configure(font=("Times New Roman", 12, "bold"))
    Time_checkin.place(relx=Xx-0.009, rely=Yy, anchor='c')

    h_checkin.place(relx=Xx+0.15, rely=Yy-0.01, height=20, width=30)
    h_checkin.insert(0,insert_content.split('|')[0])
    lb_h_checkin = tk.Label(root, text='h ',font=("Times New Roman", 12, "bold"),foreground="blue")
    lb_h_checkin.place(relx=Xx+0.2, rely=Yy, anchor='c')

    min_checkin.place(relx=Xx+0.25, rely=Yy-0.01, height=20, width=30)
    min_checkin.insert(0,insert_content.split('|')[1])
    lb_min_checkin = tk.Label(root, text='min',font=("Times New Roman", 12, "bold"),foreground="blue")
    lb_min_checkin.place(relx=Xx+0.3, rely=Yy, anchor='c')

    # ======= Check Out ==============================#
    h_checkout = tk.Entry(root,font=("Times New Roman", 12),foreground="red")
    min_checkout = tk.Entry(root,font=("Times New Roman", 12),foreground="red")

    Time_checkout = tk.Label(root, text='Thời gian ra: ',font=("Times New Roman", 12, "bold"),foreground="blue")
    Time_checkout.place(relx=Xx-0.012, rely=Yy+0.06, anchor='c')

    h_checkout.place(relx=Xx+0.15, rely=Yy+0.05, height=20, width=30)
    h_checkout.insert(0, insert_content.split('|')[2])
    lb_h_checkout = tk.Label(root, text='h ',font=("Times New Roman", 12, "bold"),foreground="blue")
    lb_h_checkout.place(relx=Xx+0.2, rely=Yy+0.06, anchor='c')

    min_checkout.place(relx=Xx+0.25, rely=Yy+0.05, height=20, width=30)
    min_checkout.insert(0,insert_content.split('|')[3])
    lb_min_checkout = tk.Label(root, text='min',font=("Times New Roman", 12, "bold"),foreground="blue")
    lb_min_checkout.place(relx=Xx+0.3, rely=Yy+0.06, anchor='c')


    # ======= Send Email checkin ===========================#
    h_sendEmail_checkin = tk.Entry(root,font=("Times New Roman", 12),foreground="red")
    min_sendEmail_checkin = tk.Entry(root,font=("Times New Roman", 12),foreground="red")

    Time_sendEmail_checkin = tk.Label(root, text='Thời gian gửi mail vào: ',font=("Times New Roman", 12, "bold"),foreground="blue")
    Time_sendEmail_checkin.place(relx=Xx+0.028, rely=Yy+0.12, anchor='c')

    h_sendEmail_checkin.place(relx=Xx+0.15, rely=Yy+0.11, height=20, width=30)
    h_sendEmail_checkin.insert(0, insert_content.split('|')[4])
    lb_h_sendEmail_checkin = tk.Label(root, text='h ',font=("Times New Roman", 12, "bold"),foreground="blue")
    lb_h_sendEmail_checkin.place(relx=Xx+0.2, rely=Yy+0.12, anchor='c')

    min_sendEmail_checkin.place(relx=Xx+0.25, rely=Yy+0.11, height=20, width=30)
    min_sendEmail_checkin.insert(0, insert_content.split('|')[5])
    lb_min_sendEmail_checkin = tk.Label(root, text='min',font=("Times New Roman", 12, "bold"),foreground="blue")
    lb_min_sendEmail_checkin.place(relx=Xx+0.3, rely=Yy+0.12, anchor='c')

    # ======= Send Email checkout ===========================#
    h_sendEmail_checkout = tk.Entry(root,font=("Times New Roman", 12),foreground="red")
    min_sendEmail_checkout = tk.Entry(root,font=("Times New Roman", 12),foreground="red")

    Time_sendEmail_checkout = tk.Label(root, text='Thời gian gửi mail ra: ',font=("Times New Roman", 12, "bold"),foreground="blue")
    Time_sendEmail_checkout.place(relx=Xx+0.022, rely=Yy+0.18, anchor='c')

    h_sendEmail_checkout.place(relx=Xx+0.15, rely=Yy+0.17, height=20, width=30)
    h_sendEmail_checkout.insert(0,insert_content.split('|')[6])
    lb_h_sendEmail = tk.Label(root, text='h ',font=("Times New Roman", 12, "bold"),foreground="blue")
    lb_h_sendEmail.place(relx=Xx+0.2, rely=Yy+0.18, anchor='c')

    min_sendEmail_checkout.place(relx=Xx+0.25, rely=Yy+0.17, height=20, width=30)
    min_sendEmail_checkout.insert(0, insert_content.split('|')[7])
    lb_min_sendEmail_checkout = tk.Label(root, text='min',font=("Times New Roman", 12, "bold"),foreground="blue")
    lb_min_sendEmail_checkout.place(relx=Xx+0.3, rely=Yy+0.18, anchor='c')

    # ======= Export File Excel ===========================#
    h_exportExcel = tk.Entry(root,font=("Times New Roman", 12),foreground="red")
    min_exportExcel = tk.Entry(root,font=("Times New Roman", 12),foreground="red")

    Time_exportExcel = tk.Label(root, text='Thời gian xuất file : ',font=("Times New Roman", 12, "bold"),foreground="blue")
    Time_exportExcel.place(relx=Xx+0.015, rely=Yy+0.24, anchor='c')

    h_exportExcel.place(relx=Xx+0.15, rely=Yy+0.23, height=20, width=30)
    h_exportExcel.insert(0,insert_content.split('|')[8])
    lb_h_exportExcel = tk.Label(root, text='h ',font=("Times New Roman", 12, "bold"),foreground="blue")
    lb_h_exportExcel.place(relx=Xx+0.2, rely=Yy+0.24, anchor='c')

    min_exportExcel.place(relx=Xx+0.25, rely=Yy+0.23, height=20, width=30)
    min_exportExcel.insert(0, insert_content.split('|')[9])
    lb_min_exportExcel = tk.Label(root, text='min',font=("Times New Roman", 12, "bold"),foreground="blue")
    lb_min_exportExcel.place(relx=Xx+0.3, rely=Yy+0.24, anchor='c')

    # ======= Link File Excel ===========================#
    file_excel = tk.Entry(root,foreground="purple")

    label_fileExcel = tk.Label(root, text='File Excel: ',font=("Times New Roman", 12, "bold"),foreground="blue")
    label_fileExcel.place(relx=0.52, rely=0.6, anchor='c')

    file_excel.place(relx=0.57, rely=0.58, height=20, width=300)
    file_excel.insert(0,insert_content.split('|')[10])

    # =======Set up mail ===========================#
    MY_ADDRESS = tk.Entry(root,font=("Times New Roman", 12),foreground="purple")
    label_mail_login = tk.Label(root, text='Điền mail gửi : ',font=("Times New Roman", 12, "bold"),foreground="blue")
    label_mail_login.place(relx=Xx-0.0057, rely=0.16, anchor='c')
    MY_ADDRESS.place(relx=0.25, rely=0.15, height=20, width=200)
    MY_ADDRESS.insert(0,insert_content.split('|')[11])

    PASSWORD = tk.Entry(root,show="*",font=("Times New Roman", 12),foreground="purple")
    label_pass_login = tk.Label(root, text='Điền mật khẩu : ',font=("Times New Roman", 12, "bold"),foreground="blue")
    label_pass_login.place(relx=Xx-0.002, rely=0.22, anchor='c')
    PASSWORD.place(relx=0.25, rely=0.21, height=20, width=200)
    PASSWORD.insert(0, insert_content.split('|')[12])

    your_host = tk.Entry(root,font=("Times New Roman", 12),foreground="purple")
    label_your_host = tk.Label(root, text='Điền Host: ',font=("Times New Roman", 12, "bold"),foreground="blue")
    label_your_host.place(relx=Xx-0.023, rely=0.28, anchor='c')
    your_host.place(relx=0.25, rely=0.27, height=20, width=200)
    your_host.insert(0,insert_content.split('|')[13])

    your_port = tk.Entry(root,font=("Times New Roman", 12),foreground="red")
    label_your_port = tk.Label(root, text='Điền  Port: ',font=("Times New Roman", 12, "bold"),foreground="blue")
    label_your_port.place(relx=Xx-0.023, rely=0.34, anchor='c')
    your_port.place(relx=0.25, rely=0.33, height=20, width=50)
    your_port.insert(0, insert_content.split('|')[14])

    # ======= Link Mail To ===========================#
    mail_to = tk.Entry(root,foreground="purple")

    label_mail_to = tk.Label(root, text='Email To: ',font=("Times New Roman", 12, "bold"),foreground="blue")
    label_mail_to.place(relx=0.52, rely=0.7, anchor='c')

    mail_to.place(relx=0.57, rely=0.68, height=20, width=300)
    mail_to.insert(0,insert_content.split('|')[15])

    # ======= Link Mail CC ===========================#
    mail_cc = tk.Entry(root,foreground="purple")

    label_mail_cc = tk.Label(root, text='Email CC: ',font=("Times New Roman", 12, "bold"),foreground="blue")
    label_mail_cc.place(relx=0.52, rely=0.8, anchor='c')

    mail_cc.place(relx=0.57, rely=0.78, height=20, width=300)
    mail_cc.insert(0, insert_content.split('|')[16])

    # ======= Button Main ===========================#
    btn = tk.Button(root, text='START', command=main,font=("Times New Roman", 13, "bold"),foreground="blue")
    btn.place(relx=.15, rely=.05,height=40, width=80, anchor='c')
    btnstop = tk.Button(root, text='STOP', command=btn_stop,font=("Times New Roman", 13, "bold"),foreground="blue")
    btnstop.place(relx=.1, rely=.05,height=40, width=80, anchor='c')
    btnstop.place_forget()

    # ======= Button Camera ===========================#
    # Camera = tk.Button(root, text='CAMERA', command=call_camera)
    # Camera.place(relx=.18, rely=.5,height=30, width=60, anchor='c')

    # ======= Button Exit ===========================#
    exitbt = tk.Button(root, text='Exit', command=exit,font=("Times New Roman", 13, "bold"),foreground="blue")
    exitbt.place(relx=.3, rely=.05, height=40, width=80, anchor='c')

    # ========panel camera ==========================
    panelimg = tk.Label()
    panelimg.place(relx=.62, rely=.08)

    # ======= Button SAVE_IMAGE ===========================#
    # CAPT = tk.Button(root, text='SAVE_IMAGE', command=save_image)
    # CAPT.place(relx=.18, rely=.6, height=30, width=80, anchor='c')

    # cap = cv2.VideoCapture(link_camera.get())
    #
    # call_camera()
    # control_voice()
    # ======= Button New Main ===========================#
    addmember = tk.Button(root, text='Thêm thành viên', command=add_mem,font=("Times New Roman", 12, "bold"),foreground="blue")
    addmember.place(relx=.26, rely=.8, height=30, width=150, anchor='c')

    # ======= insert value ===========================#
    load_logo('./vbpo-logo.png')

    # kick off the GUI
    root.mainloop()