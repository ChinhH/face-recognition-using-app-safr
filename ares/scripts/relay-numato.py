#!/usr/bin/python

from __future__ import print_function
import serial
import glob
import time
import sys
import os
import portalocker
import tempfile
import logging
from logging.handlers import TimedRotatingFileHandler
import platform

# setup
serialBaud=115200
scriptname=os.path.basename(sys.argv[0])

# configure logging
logger = logging.getLogger("Rotating Log")
logPath = "./relay-numato.log"
logFormatter = logging.Formatter('%(asctime)s [%(threadName)-12.12s] [%(levelname)-5.5s] %(message)s')
# configure file logging
fileHandler = TimedRotatingFileHandler(logPath, when="d", interval=1, backupCount=14)
fileHandler.setFormatter(logFormatter)
logger.addHandler(fileHandler)
# configure console logging
consoleHandler = logging.StreamHandler()
consoleHandler.setFormatter(logFormatter)
logger.addHandler(consoleHandler)
# set logging level
logger.setLevel(logging.INFO)
# uncomment to enable debug
#logger.setLevel(logging.DEBUG)

# verify arguments
if (len(sys.argv) > 4 or len(sys.argv) < 3):
    print("Usage: " + scriptname + " RELAYNUM DELAY [tty.device]")
    print("Eg: " + scriptname + " 0 5")
    print(" " + scriptname + " 0 5 /dev/tty.usbmodem1411")
    sys.exit(1)

# set variables from arguments
relayNum = sys.argv[1] # the relay to trigger, starting at 0 
delay = int(sys.argv[2]) # how long to trigger the relay in seconds

#send command to relay and verify state
def relaySendAndVerify(relayNumber, relayCommand):
    try:
        relayCommandString="\r" + "relay " + relayCommand + " " + str(relayNumber) + "\r"
        # create a temporary file for locking
        with portalocker.Lock(lockFile, timeout=5):
            with serial.Serial(serialPort, serialBaud, timeout=.05) as serPort:
                if serPort.isOpen():
                    #send the relay command
                    serPort.write(relayCommandString.encode())
                    serPort.flush()
                    time.sleep(.01)
                    relayVerifyCommandString="\r" + "relay read " + str(relayNumber) + "\r"
                    serPort.reset_input_buffer() # clear previous data
                    serPort.write(relayVerifyCommandString.encode())
                    serPort.flush()
                    time.sleep(.01)
                    outputChars = ""
                    while 1:
                        ch = serPort.readline().decode()
                        if len(ch) == 0:
                            break
                        outputChars += ch
                    if relayCommand in outputChars and ">" in outputChars:
                        return True
                    else:
                        return False
    except serial.SerialException as ex:
        logger.error(str(relayNum) + 'Port {0} is unavailable: {1}'.format(serialPort, ex))
        return False

#send commmand to relay
def relaySend(relayCommand):
    try:
        # create a temporary file for locking
        with portalocker.Lock(lockFile, timeout=5):
            with serial.Serial(serialPort, serialBaud, timeout=1) as serPort:
                if serPort.isOpen():
                    #send the relay command
                    serPort.write(relayCommand.encode())
                    serPort.flush()
                    time.sleep(.01)
    except serial.SerialException as ex:
        logger.error(str(relayNum) + 'Port {0} is unavailable: {1}'.format(serialPort, ex))
        exit()

#verify it's a Numato relay device
def testDevice(serDevice):
    try:
        # create a temporary file for locking
        with portalocker.Lock(lockFile, timeout=5):
            with serial.Serial(serDevice, serialBaud, timeout=.05) as serPort:
                if serPort.isOpen():
                    #send the relay command
                    serPort.write("\rid get\r".encode())
                    serPort.flush()
                    time.sleep(.01)
                    outputChars = ""
                    while 1:
                        ch = serPort.readline().decode()
                        if len(ch) == 0:
                            break
                        outputChars += ch
                    if "id get" in outputChars and ">" in outputChars:
                        logger.debug("Expected output found in relay.")
                        return True
                    else:
                        return False
    except serial.SerialException as ex:
        return False

def relayOn():
    #turn on the relay
    relaySend("\rrelay on " + str(relayNum) + "\r")

def relayOff():
    #turn off the relay
    i=0
    maxTries=5
    while relaySendAndVerify(relayNum, "off")==False and i <= maxTries:
        logger.warn("Relay " + str(relayNum) + " still enabled! - Retry #" + str(i) + ".")
        i=i+1
    
# build list of possible relay devices and set lockfile
systemPlatform=platform.system().lower()
if systemPlatform in ("linux", "linux2"):
    # linux
    serialPortsGlob = '/dev/ttyACM*'
    serialPorts = glob.glob(serialPortsGlob)
    lockFile=tempfile.gettempdir()+"/.numato_lockfile"
elif systemPlatform == "darwin":
    # MAC OS X
    serialPortsGlob = '/dev/tty.usbmodem*'
    serialPorts = glob.glob(serialPortsGlob)
    lockFile=tempfile.gettempdir()+"/.numato_lockfile"
elif systemPlatform == "windows":
    # Windows
    serialPorts = []
    for i in range(1,257):
        serialPorts.append("COM" + str(i))
    lockFile=tempfile.gettempdir()+"\\.numato_lockfile"
else:
    logger.error("Failed to determine Serial Ports?!")

#determine serial device and test it
if (len(sys.argv) == 4): # serial device passed as arg
    serialPort = sys.argv[3]
    logger.info("Using Serial Port: " + serialPort)
    #test serial port
    try:
        s = serial.Serial(serialPort)
        s.close()
    except serial.SerialException:
        logger.error('Exception accessing serial device ' + serialPort)
        sys.exit(99)
else: # determine device 
    if (len(serialPorts) == 0):
        logger.error('No compatible serial devices found at: ' + serialPortsGlob)
        sys.exit(98)
    else:
        if systemPlatform == "windows":
            serialPort = False
            for device in serialPorts: # loop through devices
                if testDevice(device): # check if it's a numato device
                    serialPort=device # use it if so
                    break
            if not serialPort:
                #no serial device detected!
                logger.error('No compatible serial devices found on any COM ports!')
                sys.exit(97)
        else:
            #use first device in list if not specified
            serialPort = serialPorts[0]

# turn on the relay
logger.info("Starting Relay Action.")
logger.info('Opening device ' + str(serialPort) + ' relay ' + str(relayNum) + ' for ' + str(delay) + ' seconds.')
relayOn()

# sleep for delay
for i in range(delay):
    time.sleep(1)
else:
    #turn off the relay
    relayOff()
    logger.info('Closing device ' + str(serialPort) + ' relay ' + str(relayNum) + ' after ' + str(delay) + ' seconds.')

# finished
logger.info("Finished Relay Action.")
