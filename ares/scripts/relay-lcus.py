#!/usr/bin/python

import serial
import time
import sys
import argparse
import logging
import glob
import platform
import tempfile
import portalocker
import codecs

# define messages
relayOnHex = 'A00101A2'
relayOffHex = 'A00100A1'

# read arguments
parser = argparse.ArgumentParser(description='SAFR LCUS relay tool')
parser.add_argument('delay', nargs='?', default=5, type=int, action='store', metavar='delay', help='Number of seconds to trigger relay.')
parser.add_argument('-p', '--port', action='store', help='Serial port device - COM1, /dev/tty.usb-serial, etc.')
parser.add_argument('-b', '--baud', action='store', choices=['9600'], help='Serial port baud rate.')
parser.add_argument('-r', '--relay', action='store', choices=[1,2,3,4,], default=1, type=int, help='Relay device number.')
parser.add_argument('-d', '--debug', action='store_true', help='Enable DEBUG level logging.')
parser.add_argument('-q', '--quiet', action='store_true', help='Suppress non-error output.')
args = parser.parse_args()

# setup logging
logging.basicConfig(
   format='%(asctime)s %(levelname)-8s %(message)s',
   level=logging.INFO,
   datefmt='%Y-%m-%d %H:%M:%S')

# parse arguments
if args.quiet:
    logging.getLogger().setLevel(logging.CRITICAL)
if args.debug:
    logging.debug('Enabling debug logging.')
    logging.getLogger().setLevel(logging.DEBUG)
if args.delay:
    delay = args.delay
    logging.debug('Delay set to ' + str(delay) + ' seconds.')
if args.port:
    serialPort = args.port
    logging.debug('Serial Port specified as: ' + serialPort)
else:
    serialPort = None
if args.baud:
    serialBaud = args.baud
    logging.debug('Serial Port baud rate specified as: ' + serialBaud)
else:
    serialBaud = 9600

# build list of possible relay devices and set lockfile
systemPlatform = platform.system().lower()
if systemPlatform in ('linux', 'linux2'):
    # linux
    serialPortsGlob = '/dev/ttyACM*'
    serialPorts = glob.glob(serialPortsGlob)
    lockFile = tempfile.gettempdir() + '/.lcus_lockfile'
elif systemPlatform == 'darwin':
    # MacOS
    serialPortsGlob = '/dev/tty.usbserial-*'
    serialPorts = glob.glob(serialPortsGlob)
    lockFile = tempfile.gettempdir() + '/.lcus_lockfile'
elif systemPlatform == 'windows':
    # Windows
    serialPorts = []
    for i in range(1,257):
        serialPorts.append('COM' + str(i))
    lockFile = tempfile.gettempdir() + '\\.lcus_lockfile'
else:
    logging.error('Failed to determine Serial Ports?!')

logging.debug('Serial ports available: ' + str(serialPorts))    

# determine serial device and test it
if serialPort: 
    logging.info('Using Serial Port: ' + serialPort)
    # test serial port
    try:
        s = serial.Serial(serialPort)
        s.close()
    except serial.SerialException:
        logging.error('Exception accessing serial device ' + serialPort)
        sys.exit(99)
    except Exception as e:
        logging.error('Exception with serial device ' + serialPort)
        logging.debug(str(e))
        sys.exit(99)
else: # determine device
    if (len(serialPorts) == 0):
        logging.error('No compatible serial devices found at: ' + serialPortsGlob)
        sys.exit(98)
    else:
        # use first device in list if not specified
        serialPort = serialPorts[0]

#send commmand to relay
def relaySend(relayCommand):
    try:
        # create a temporary file for locking
        with portalocker.Lock(lockFile, timeout=5):
            with serial.Serial(serialPort, serialBaud, timeout=1) as serPort:
                if serPort.isOpen():
                    #send the relay command
                    ret = serPort.write(relayCommand)
                    serPort.flush()
                    time.sleep(.01)
                    if ret == 4:
                        return True
                    else:
                        logger.error('Could not send command ' + relayCommand + ' to relay!')
                        exit(2)
    except serial.SerialException as ex:
        logger.error(str(relayNum) + 'Port {0} is unavailable: {1}'.format(serialPort, ex))
        exit(1)
        
def relayOn():
    relaySend(codecs.decode(relayOnHex, 'hex'))

def relayOff():
    relaySend(codecs.decode(relayOffHex, 'hex'))
    
logging.info('Triggering relay for ' + str(delay) + ' seconds.')
# activate
relayOn()

# sleep for delay
for i in range(delay):
    time.sleep(1)
else:
    # deactivate
    relayOff()
    logging.info('Done, deactivating relay.')


