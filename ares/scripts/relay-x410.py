#!/usr/bin/python
# Copyright 2018, RealNetworks, Inc

import os
import sys
import requests
import logging
import base64

#check for args
if (len(sys.argv) != 3):
  scriptName=os.path.basename(sys.argv[0])
  print("Usage: " + scriptName + " relayHost relayNumber")
  print("Eg: " + scriptName + " 192.168.1.2 1")
  sys.exit(1)
else:
  #set hostname from first arg
  hostName = sys.argv[1]
  relayNumber = sys.argv[2]

#setup
protocol="https://"
baseURL=protocol + hostName
url="/state.json"
username="user"
password="webrelay"

#configure logging
logging.basicConfig(
    format='%(asctime)s %(levelname)-8s %(message)s',
    level=logging.DEBUG,
    datefmt='%Y-%m-%d %H:%M:%S')
logfh = logging.FileHandler('relay-x410.log')
logfh.setLevel(logging.DEBUG)

# verify the port is listening
def portCheck(name,port):
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            s.connect((hostName, int(port)))
            s.shutdown(2)
            printResults(name,"pass")
            return True
        except:
            printResults(name,"fail")
            return False
    except:
        printResults(name,"other")
        pass

# must find searchString in response
def sendRelayCommand(relayNum):
    try:
        Params = {'relay' + relayNum:'2'} # relay to activate and "2" for "pulse"
        logging.debug("Requesting URL: " + baseURL + url)
        with requests.get(baseURL + url, params=Params, auth=(username, password), verify=False, timeout=5) as response:
            logging.debug("Status code: " + str(response.status_code))
            if 200 <= response.status_code <= 299:
                logging.debug(response.json())
                responseData = response.json()
                if responseData['relay' + str(relayNum)] == '1': # check for '1' in response JSON, meaning relay is activated
                    logging.info("Relay " + relayNum + " Command Successful.")
                    return True
                else:
                    logging.info("Relay " + relayNum + " Command Unuccessful - Response value check.")
                    return False
                logging.debug("Status code: " + str(response.status_code))
            else:
                logging.info("Relay " + relayNum + " Command Unuccessful - Response Code.")
                return False
    except (requests.exceptions.ConnectionError, requests.exceptions.Timeout, requests.exceptions.ProxyError) as e:
        logging.info("Relay " + relayNum + " Command Unuccessful - HTTP error.")
        logging.debug("Exception: " + str(e))
        return False
    except:
        e = sys.exc_info()[0]
        logging.debug("Exception: " + str(e))
        logging.info("Relay " + relayNum + " Command Unuccessful - Other.")
        pass

logging.info("SAFR x410 Relay Control")
logging.info("-------------------------------")
sendRelayCommand(relayNumber)

