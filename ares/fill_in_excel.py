import pandas as pd
from pandas import ExcelWriter
from openpyxl import load_workbook
path = 'C:/Users/VBPO-HC/Desktop/tmp.xlsx'
import openpyxl
dfOutput1 = pd.read_excel(path,dtype=object)
# dfOutput1 = pd.read_excel(path, sheet_name='DATA', dtype=object, header = 3, usecols='A:V')
# print(dfOutput1)
ACC = 'HaHT'
Checkin = "9/4/2019 08:00:00 AM"
Checkout = "9/4/2019 18:00:00 PM"

tmp_row = 0

for i in range(0,1000):
    if str(dfOutput1['ACC'][i]) == 'nan':
        dfOutput1.iat[i, 3] = ACC

        dfOutput1.iat[i, 1] = Checkin
        dfOutput1.iat[i, 11] = Checkout
        tmp_row = i
        # print(tmp_row)
        break
for i in range(0,2000):
    dfOutput1.iat[i, 2] = ('=IF(D{}="",IF(B{}="","","NGUOI_LA"),VLOOKUP(D{},base!$B$2:$J$101,2,0))').format(i + 2,
                                                                                                            i + 2,
                                                                                                            i + 2)
    dfOutput1.iat[i, 4] = ('=IF(D{}="","",VLOOKUP(D{},base!$B$2:$J$101,3,0))').format(i + 2, i + 2)
    dfOutput1.iat[i, 5] = ('=IF(C{}="NGUOI_LA","NO",IF(D{}="","",VLOOKUP(D{},base!$B$2:$J$101,4,0)))').format(i + 2,
                                                                                                              i + 2,
                                                                                                              i + 2)
    dfOutput1.iat[i, 6] = ('=IF(C{}="NGUOI_LA","NO",IF(D{}="","",VLOOKUP(D{},base!$B$2:$J$101,5,0)))').format(i + 2,
                                                                                                              i + 2,
                                                                                                              i + 2)
    dfOutput1.iat[i, 7] = ('=IF(C{}="NGUOI_LA","NO",IF(D{}="","",VLOOKUP(D{},base!$B$2:$J$101,6,0)))').format(i + 2,
                                                                                                              i + 2,
                                                                                                              i + 2)
    dfOutput1.iat[i, 8] = ('=IF(D{}="","",VLOOKUP(D{},base!$B$2:$J$101,7,0))').format(i + 2, i + 2)
    dfOutput1.iat[i, 9] = ('=IF(C{}="NGUOI_LA",O{},IF(D{}="","",VLOOKUP(D{},base!$B$2:$J$101,8,0)))').format(i + 2,
                                                                                                             i + 2,
                                                                                                             i + 2,
                                                                                                             i + 2)
    dfOutput1.iat[i, 10] = ('=IF(C{}="NGUOI_LA",P{},IF(D{}="","",VLOOKUP(D{},base!$B$2:$J$101,9,0)))').format(i + 2,
                                                                                                              i + 2,
                                                                                                              i + 2,
                                                                                                              i + 2)
book = load_workbook(path)
writer = ExcelWriter(path, engine='openpyxl')
writer.book = book
writer.sheets = dict((ws.title, ws) for ws in book.worksheets)
dfOutput1.to_excel(writer,'EXPORT',index=False,startrow=1,header=False)
writer.save()

print('OK')

