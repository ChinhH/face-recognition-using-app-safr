import os
import cv2
import time
import datetime
import win32com.client as win32
from gtts import gTTS
import pandas as pd
from pandas import ExcelWriter
from openpyxl import load_workbook
from selenium import webdriver
import automagica
from automagica import *
from selenium.webdriver.common.keys import Keys
# print(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
m = datetime.datetime.now().strftime("%m")
d = datetime.datetime.now().strftime("%d")
H = datetime.datetime.now().strftime("%H")
M = datetime.datetime.now().strftime("%M")
S = datetime.datetime.now().strftime("%S")
# print(d)
# print(m)
# print(H)
# print(M)
path_in = "./ares.log"
path_exl = 'C:/Users/VBPO-HC/Desktop/tmp.xlsx'
path_save = ""
hr_check = 8
min_check = 5

# ---------------------
DANHSACH = pd.read_excel(path_exl, sheet_name='base', dtype=object)
# print(DANHSACH['ACC'][0])
DS_Member1 = []
DS = []
for i in range(len(DANHSACH['ACC'])):
    if str(DANHSACH['ACC'][i]) != 'nan':
        DS.append(DANHSACH['ACC'][i])
        DS_Member1.append([DANHSACH['ACC'][i],0])
# print(DS_Member1)
# print(DS)
# list = 'TrungPM,ChinhH,PhungPD,HuyPT,TuT,VietHN,HaiPM,HuyTran,HaHT,HuongNTT,TranBui,Ngoc_TBK,BinhLe'
#
# DS = list.split(',')
# DS_Member = []
# for i in range(len(DS)):
#     DS_Member.append([DS[i],0])

def checkin_checkout(acc_member,file_log):
    with open(file_log) as f:
        time_in = ""
        time_out =""
        for line in f:
            try:
                if str(line.split(' ')[0]) == 'XXX':
                    mon = line.split('-')[1]
                    day = line.split('-')[2].split(' ')[0]
                    hr = line.split(':')[0].split(' ')[2]
                    min = line.split(':')[1]
                    se = line.split(':')[2].split(' ')[0]
                    if m == mon and d == day:
                        ACC = line.split(' ')[6]
                        if int(hr) < hr_check or (int(hr) == hr_check and int(min) <= min_check):
                            if ACC == str(acc_member[0]):
                                if int(acc_member[1]) == 0:
                                    time_in = "2019-" + mon+'-' + day + " "+hr + ":" + min + ":"+ se
                                    acc_member[1] = 1
                        else:
                            if ACC == str(acc_member[0]):
                                if int(acc_member[1]) == 0:
                                    time_in = "2019-" + mon+'-' + day + " "+hr + ":" + min + ":"+ se
                                    acc_member[1] = 1

                        if ACC == str(acc_member[0]):
                            time_out = "2019-" + mon+'-' + day + " "+hr + ":" + min + ":"+ se
                            acc_member[1] = 1
            except:
                pass
    if int(acc_member[1]) == 1:
        acc_member[1] = 0
        fill_file_excel(path_exl,str(acc_member[0]),time_in,time_out)

def get_checkin_member(acc_member, string, file_log):
    with open(file_log) as f:
            for line in f:
                try:
                    if str(line.split(' ')[0]) == 'XXX':
                        mon = line.split('-')[1]
                        day = line.split('-')[2].split(' ')[0]
                        hr = line.split(':')[0].split(' ')[2]
                        min = line.split(':')[1]
                        # print('acc_member[1]',acc_member[1])
                        if m == mon and d == day:
                            ACC = line.split(' ')[6]
                            if int(hr) < hr_check or (int(hr) == hr_check and int(min) <= min_check):
                                if ACC == str(acc_member[0]):
                                    if int(acc_member[1]) == 0:
                                        string = string + str(acc_member[0])+": checkin success ! " + "Time: " + hr + ":" + min + '\n'
                                        acc_member[1] = 1
                            else:
                                if ACC == str(acc_member[0]):
                                    if int(acc_member[1]) == 0:
                                        string = string + str(acc_member[0]) + ": checkin Too Late ! " + "Time: " + hr + ":" + min + '\n'
                                        acc_member[1] = 1

                except:
                    pass
    if int(acc_member[1]) == 0:
        string = string + str(acc_member[0]) + ": absence ! " + '\n'
    else:
        acc_member[1] = 0
    return string

def get_checkout_member(acc_member, string, file_log):
    sttr = ''
    with open(file_log) as f:
            for line in f:
                try:
                    if str(line.split(' ')[0]) == 'XXX':
                        mon = line.split('-')[1]
                        day = line.split('-')[2].split(' ')[0]
                        hr = line.split(':')[0].split(' ')[2]
                        min = line.split(':')[1]
                        if m == mon and d == day:
                            ACC = line.split(' ')[6]
                            if ACC == str(acc_member[0]):
                                sttr = str(acc_member[0])+ "___Time checkout : " + hr + ":" + min + '\n'
                                acc_member[1] = 1
                except:
                    pass
    if int(acc_member[1]) == 0:
        sttr = str(acc_member[0]) + ": absence ! " + '\n'
    else:
        acc_member[1] = 0
    return string+sttr

def voice(acc):
    mytext = 'Hello'+ str(acc) + '   Welcome to VBPO!'
    # Language in which you want to convert
    # language = 'vi'
    language = 'en'
    myobj = gTTS(text=mytext, lang=language, slow=False)

    # Saving the converted audio in a mp3 file named
    # welcome
    myobj.save("welcome.mp3")

    # Playing the converted file
    os.system("welcome.mp3")
#---------sign-------------------------------------
sign = "\n"+"\n"+"-------------------------------------" + "\n" + "HUYNH CHINH" + "\n" + "V.B.P.O Joint Stock Company" + "\n" +'12F Danang Software Park, 02 Quang Trung St., Danang City, Vietnam'+'\n'+'Tel: 084-2363-6510087'+'\n'+'Website: www.vbpo.com.vn'+'\n'+'IMPORTANT NOTICE'+'\n'+'The information in this e-mail and any attached files is CONFIDENTIAL and may be legally privileged or prohibited from disclosure and unauthorized use. The views of the author may not necessarily reflect those of the Company. It is intended solely for the addressee, or the employee or agent responsible for delivering such materials to the addressee. If you have received this message in error please return it to the sender then delete the email and destroy any copies of it. If you are not the intended recipient, any form of reproduction, dissemination, copying, disclosure, modification, distribution and/or publication or any action taken or omitted to be taken in reliance upon this message or its attachments is prohibited and may be unlawful. At present the integrity of e-mail across the Internet cannot be guaranteed and messages sent via this medium are potentially at risk. All liability is excluded to the extent permitted by law for any claims arising as a result of the use of this medium to transmit information by or to V.B.P.O JSC'+'\n' +'Please consider the environment before printing this email'+'\n'
# print(sign)
   
def send_Email_Outlook(message,sign):
    outlook = win32.Dispatch('outlook.application')
    mail = outlook.CreateItem(0)
    mail.To = 'phan.dinh.phung@vbpo.com.vn'
    mail.CC = 'huynh.chinh@vbpo.com.vn'
    mail.Subject = 'Tracking time in/out member DRI'
    mail.Body = message + sign
    mail.Send()
    print('Done Send email')

def main_speech():
    while True:
        # time.sleep(0.1)
        m = datetime.datetime.now().strftime("%m")
        d = datetime.datetime.now().strftime("%d")
        H = datetime.datetime.now().strftime("%H")
        M = datetime.datetime.now().strftime("%M")
        S = datetime.datetime.now().strftime("%S")
        with open(path_in) as f:
            for line in f:
                try:
                    if str(line.split(' ')[0]) == 'XXX':
                        mon = line.split('-')[1]
                        day = line.split('-')[2].split(' ')[0]
                        hr = line.split(':')[0].split(' ')[2]
                        min = line.split(':')[1]
                        se = line.split(':')[2].split(' ')[0]

                        # print('acc_member[1]',acc_member[1])
                        if m == mon and d == day and H == hr and M == min and S == se:
                            ACC = line.split(' ')[6]
                            if ACC != 'm':

                                #print(ACC)
                                if ACC not in DS:
                                    # print('imlang')
                                    break
                                else:
                                    print('noi')
                                    voice(ACC)
                                    # time.sleep(10)
                                    # break
                except Exception as aaa:
                    print(aaa)
                    pass

def capt_img_url(url,dirname):
    cap = cv2.VideoCapture(url)
    print(url)
    sumline = 0
    linenow = 0
    arrline = []
    lstin = []
    lstintime = []
    lstout = []
    lstoutime = []
    abccc = False
    print(cap.isOpened())
    while (cap.isOpened()):
        time.sleep(0.01)
        m = datetime.datetime.now().strftime("%m")
        d = datetime.datetime.now().strftime("%d")
        H = datetime.datetime.now().strftime("%H")
        M = datetime.datetime.now().strftime("%M")
        S = datetime.datetime.now().strftime("%S")
        with open(path_in) as f:
            arrline = f.readlines()
            linenow = len(arrline)
            # print('11111111111')
            # print(sumline)
            # print('22222222222')
            # print(linenow)
        if linenow > sumline:
            for i in range(sumline,linenow):
                line = arrline[i]
                # try:
                if str(line.split(' ')[0]) == 'XXX':
                    mon = line.split('-')[1]
                    day = line.split('-')[2].split(' ')[0]
                    hr = line.split(':')[0].split(' ')[2]
                    min = line.split(':')[1]
                    se = line.split(':')[2].split(' ')[0]
                    # print('vao10')
                    # print('acc_member[1]',acc_member[1])
                    # print(S)
                    # print(se)
                    # if m == mon and d == day and H == hr and M == min and S == se:
                    #     print(se)
                    #     print(S)
                        # print('vao11')
                    ACC = line.split(' ')[6]

                    if ACC != 'm' and ACC != 'f' and ACC != "m\n":
                        indexacc = -1
                        try:
                            indexacc = lstin.index(str(ACC))
                        except:
                            indexacc = -1
                            pass
                        if indexacc < 0:
                            lstin.append(ACC)
                            lstintime.append(str(hr)+str(min)+str(se))
                            lstout.append(ACC)
                            lstoutime.append(str(hr) + str(min) + str(se))
                            ret, frame = cap.read()
                            if not ret:
                                print('err')
                                break
                            else:
                                print('chuanbi')
                                name = "rec_frame-" + str(ACC) + "_In" + ".jpg"
                                cv2.imwrite(os.path.join(dirname, name), frame)
                        else:
                            if int(str(hr)+str(min)+str(se)) - int(lstoutime[indexacc]) > 2:
                                lstoutime[indexacc] = str(hr)+str(min)+str(se)
                                ret, frame = cap.read()
                                if not ret:
                                    print('err')
                                    break
                                else:
                                    print('chuanbi')
                                    name = "rec_frame-" + str(ACC) + "Out" + ".jpg"
                                    cv2.imwrite(os.path.join(dirname, name), frame)

                    # print('acc_member[1]', ACC)
                    # if ACC != 'm'or ACC != 'f':
                    #     ret, frame = cap.read()
                    #     if not ret:
                    #         print('err')
                    #         break
                    #     else:
                    #         print('chuanbi')
                    #         name = "rec_frame" + str(i) + ".jpg"
                    #         cv2.imwrite(os.path.join(dirname, name), frame)
                    #         i += 1

                            # ret, frame = cap.read()
                            # name = "rec_frame_" + str(ACC) + '_' + str(min) + str(hr) + str(day) + str(mon) + ".jpg"
                            # cv2.imwrite(os.path.join(dirname, name), frame)
                            # print('done capt')
                            # break
                                # print(DS)
                                # print(ACC)
                                # if str(ACC) not in DS:
                                #     print('1')
                                #     break
                                # else:
                                #     # ret, frame = cap.read()
                                #     # print('2')
                                #     # if not ret:
                                #     #     # print('4')
                                #     #     break
                                #     # else:
                                #         # print('3')
                                #     name = "rec_frame_" + str(ACC)+'_'+str(min)+str(hr)+str(day)+str(mon) + ".jpg"
                                #     cv2.imwrite(os.path.join(dirname, name), frame)
                                #     print('done capt')
                                #     break
                # except:
                #     pass
            sumline = linenow
        # else:
        #     print(lstin)
        #     print(lstintime)
        #     print(lstoutime)

def main_send_email(hr_send,min_send):
    # ---- set time check and send email ------
    day_send = datetime.datetime.now().strftime("%d")
    hr_send = '17'
    min_send = '20'
    # -----------------------------------------q
    while True:
        time.sleep(0.1)
        m = datetime.datetime.now().strftime("%m")
        d = datetime.datetime.now().strftime("%d")
        H = datetime.datetime.now().strftime("%H")
        M = datetime.datetime.now().strftime("%M")
        S = datetime.datetime.now().strftime("%S")
        if str(H) == str(hr_send) and str(M) == str(min_send):
            if int(day_send) == int(d): # send email day
                message_in = "Tracking checkin member DRI " + str(datetime.datetime.now().strftime("%Y-%m-%d")) + "\n\n"
                for name in DS_Member1:
                    message_in = get_checkin_member(name, message_in, path_in)
                    checkin_checkout(name,path_in)
                message_out = "Tracking checkout member DRI " + str(datetime.datetime.now().strftime("%Y-%m-%d")) + "\n\n"
                for name in DS_Member1:
                    message_out = get_checkout_member(name, message_out, path_in)
                message = message_in + "\n\n"+ message_out

                print(message+sign)
                send_Email_Outlook(message,sign)
                day_send+=1

def fill_file_excel(filexl,ACC,Checkin,Checkout):
    dfOutput1 = pd.read_excel(filexl,  sheet_name='EXPORT',dtype=object)
    for i in range(0, 1000):
        if str(dfOutput1['ACC'][i]) == 'nan':
            dfOutput1.iat[i, 3] = ACC
            dfOutput1.iat[i, 1] = Checkin
            dfOutput1.iat[i, 11] = Checkout
            break
    for i in range(0, 2000):
        dfOutput1.iat[i, 2] = ('=IF(D{}="",IF(B{}="","","NGUOI_LA"),VLOOKUP(D{},base!$B$2:$J$101,2,0))').format(i + 2,
                                                                                                                i + 2,
                                                                                                                i + 2)
        dfOutput1.iat[i, 4] = ('=IF(D{}="","",VLOOKUP(D{},base!$B$2:$J$101,3,0))').format(i + 2, i + 2)
        dfOutput1.iat[i, 5] = ('=IF(C{}="NGUOI_LA","NO",IF(D{}="","",VLOOKUP(D{},base!$B$2:$J$101,4,0)))').format(i + 2,
                                                                                                                  i + 2,
                                                                                                                  i + 2)
        dfOutput1.iat[i, 6] = ('=IF(C{}="NGUOI_LA","NO",IF(D{}="","",VLOOKUP(D{},base!$B$2:$J$101,5,0)))').format(i + 2,
                                                                                                                  i + 2,
                                                                                                                  i + 2)
        dfOutput1.iat[i, 7] = ('=IF(C{}="NGUOI_LA","NO",IF(D{}="","",VLOOKUP(D{},base!$B$2:$J$101,6,0)))').format(i + 2,
                                                                                                                  i + 2,
                                                                                                                  i + 2)
        dfOutput1.iat[i, 8] = ('=IF(D{}="","",VLOOKUP(D{},base!$B$2:$J$101,7,0))').format(i + 2, i + 2)
        dfOutput1.iat[i, 9] = ('=IF(C{}="NGUOI_LA",O{},IF(D{}="","",VLOOKUP(D{},base!$B$2:$J$101,8,0)))').format(i + 2,
                                                                                                                 i + 2,
                                                                                                                 i + 2,
                                                                                                                 i + 2)
        dfOutput1.iat[i, 10] = ('=IF(C{}="NGUOI_LA",P{},IF(D{}="","",VLOOKUP(D{},base!$B$2:$J$101,9,0)))').format(i + 2,
                                                                                                                  i + 2,
                                                                                                                  i + 2,
                                                                                                                  i + 2)
    book = load_workbook(filexl)
    writer = ExcelWriter(filexl, engine='openpyxl')
    writer.book = book
    writer.sheets = dict((ws.title, ws) for ws in book.worksheets)
    dfOutput1.to_excel(writer, 'EXPORT', index=False, startrow=1, header=False)
    writer.save()
if __name__ == '__main__':
    main_send_email()
    # url = "rtsp://192.168.0.86:554/onvif1"
    # path_sv = "C:/Users/VBPO-HC/Desktop/ab"
    # capt_img_url(url,path_sv)
    # main_speech()
